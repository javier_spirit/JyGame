﻿using UnityEngine;

public class TestCast : MonoBehaviour
{
    public Camera UICamera;
    public Camera MainCamera;
    public GameObject Tree;

    
    void Start()
    {

    }

    
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            TestRay();
        }
    }

    void TestRay()
    {
        var pos1 = UICamera.ScreenToWorldPoint(Input.mousePosition);
        var pos2 = Tree.transform.position;

        var ray1 = new Ray(pos1, pos2);
        //var ray2 = UICamera.ScreenPointToRay(pos2);
        
        RaycastHit hitInfo;
        if (Physics.Raycast(ray1, out hitInfo, 10000))
        {
            Debug.Log(hitInfo);
            Debug.Log("--------------");
        }
        
        Debug.DrawLine(pos1, Vector3.down, Color.red, 5);
        Debug.DrawLine(pos1, pos2, Color.green, 5);
    }
}
