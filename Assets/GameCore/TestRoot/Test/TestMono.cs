﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestMono : MonoBehaviour
{
    private Dictionary<string, Type> _dics;
    private string _name;

    void Awake()
    {
        _name = "TestUI";
        _dics = new Dictionary<string, Type>();
        _dics.Add(_name, typeof(TestMono1));
    }

    void Start()
    {
        var t = gameObject.AddComponent(_dics[_name]);

        var tt = t as TestMono1;
        tt.MyName = "TestTTUI";
    }


    void Update()
    {

    }
}