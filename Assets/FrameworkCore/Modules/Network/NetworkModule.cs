﻿/*
 *  date: 2018-03-27
 *  author: John-chen
 *  cn: 网络模块
 *  en: todo:
 */

namespace JyFramework
{
    /// <summary>
    /// 网络模块
    /// </summary>
    public class NetworkModule : BaseModule
    {
        public NetworkModule(string name = ModuleName.Network) : base(name)
        {
            
        }

        public override void InitGame()
        {
            
        }
    }
}
