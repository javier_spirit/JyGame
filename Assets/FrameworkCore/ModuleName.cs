﻿/*
 *  date: 2018-04-21
 *  author: John-chen
 *  cn: 游戏所有模块名
 *  en: todo:
 */

namespace JyFramework
{
    /// <summary>
    /// 模块名
    /// </summary>
    public sealed class ModuleName
    {
        public const string UI          = "UI"      + "Moduel";
        public const string Data        = "Data"    + "Module";
        public const string Aduio       = "Aduio"   + "Module";
        public const string Log         = "Log"     + "Module";
        public const string Network     = "Network" + "Module";
        public const string Object      = "Object"  + "Module";
        public const string Res         = "Res"     + "Module";
        public const string Script      = "Script"  + "Module";
        public const string Thread      = "Thread"  + "Module";
        public const string Time        = "Time"    + "Module";
        public const string Util        = "Util"    + "Module";
        public const string Event       = "Event"   + "Module";
        public const string AI          = "AI"      + "Module";
    }
}
